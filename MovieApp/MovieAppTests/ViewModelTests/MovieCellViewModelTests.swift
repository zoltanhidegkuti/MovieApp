//
//  MovieCellViewModelTests.swift
//  MovieAppTests
//
//  Created by Hidegkuti Zoltán on 2022. 02. 27..
//

import Foundation

import XCTest
import Nimble
@testable import MovieApp

class MovieCellViewModelTests: XCTestCase {

	override func setUp() {
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}

	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
	}

	func testGenerateViewModelFromMovie() {
		let movie = Movie(id: 2, title: "title", overview: "overview", release_date: "2022.01.01", poster_path: "poster_path", popularity: 1800.1, vote_average: 4.3)
		testGenerateViewModel(from: movie)
	}

	private func testGenerateViewModel(from movie: Movie) {
		let viewModel = MovieCellViewModel(movie: movie)

		expect(viewModel.id).to(equal(movie.id))
		expect(viewModel.title).to(equal(movie.title))
		expect(viewModel.popularity).to(equal(movie.popularity))
		expect(viewModel.overview).to(equal(movie.overview))
		expect(viewModel.rating).to(equal(movie.vote_average))
		expect(viewModel.poster_Path).to(equal(movie.posterURL))
		expect(viewModel.release_date).to(equal(movie.release_date))
	}
	
}
