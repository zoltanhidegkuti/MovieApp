//
//  MovieTests.swift
//  MovieAppTests
//
//  Created by Hidegkuti Zoltán on 2022. 02. 27..
//

import XCTest
import Nimble
@testable import MovieApp

class MovieTests: XCTestCase {

	override func setUp() {
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}

	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
	}

	func testMovieModel() {
		let id = 007
		let title = "Test title"
		let overview = "Description 1"
		let release_date = "2022.01.01."
		let poster_path = "test poster path"
		let popularity = 1800.0
		let vote_average = 4.8

		var movie = Movie(id: id,
						  title: title,
						  overview: overview,
						  release_date: release_date,
						  poster_path: poster_path,
						  popularity: popularity,
						  vote_average: vote_average)

		// Full info
		expect(movie).toNot(beNil())
		expect(movie.id).to(equal(007))
		expect(movie.title).to(equal("Test title"))
		expect(movie.overview).to(equal("Description 1"))
		expect(movie.release_date).to(equal("2022.01.01."))
		expect(movie.poster_path).to(equal("test poster path"))
		expect(movie.popularity).to(equal(1800.0))
		expect(movie.vote_average).to(equal(4.8))
	}
}
