//
//  MovieCell.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import UIKit
import Kingfisher


public class MovieCell: UITableViewCell {
	// MARK: - Outlets
	@IBOutlet weak var posterImageView: UIImageView!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var publishLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var rating: UILabel!

	// MARK: - Setting viewModel
	var viewModel: MovieCellViewModel? {
		didSet {
			bindViewModel()
		}
	}

	// MARK: - Bindings
	private func bindViewModel() {
		if let viewModel = viewModel {
			rating.text = String(viewModel.rating)
			posterImageView.layer.masksToBounds = true
			posterImageView.layer.cornerRadius = 5
			posterImageView.kf.setImage(with: viewModel.poster_Path)
			descriptionLabel.text = viewModel.overview
			titleLabel.text = viewModel.title
			publishLabel.text = viewModel.release_date
		}
	}
}
