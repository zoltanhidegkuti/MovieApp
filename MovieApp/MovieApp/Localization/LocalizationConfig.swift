//
//  LocalizationConfig.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 25..
//

import Foundation

class Localization {
	static let searchBarPlaceholder = NSLocalizedString("movieListViewController.searchBar.placeholder", comment: "")
	static let emptyCell = NSLocalizedString("movieListViewController.emptyCell", comment: "")
	static let defaultQuery = NSLocalizedString("movieListViewController.defaultQuery", comment: "")
	static let requestFailed = NSLocalizedString("movieViewModel.requestFailed", comment: "")
	static let notFound = NSLocalizedString("movieViewModel.notFound", comment: "")
	static let bookmarkAdded = NSLocalizedString("movieDetailViewController.bookmarkAdded", comment: "")
	static let bookmarkRemoved = NSLocalizedString("movieDetailViewController.bookmarkRemoved", comment: "")
	static let bookmarkAddedSubtitle = NSLocalizedString("movieDetailViewController.bookmarkAddedSubtitle", comment: "")
	static let bookmarkRemovedSubtitle = NSLocalizedString("movieDetailViewController.bookmarkRemovedSubtitle", comment: "")
	
}
