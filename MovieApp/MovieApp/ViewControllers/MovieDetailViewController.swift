//
//  MovieDetailViewController.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import UIKit
import PKHUD
import RxSwift
import RxCocoa
import SwiftUI
import NotificationBannerSwift

class MovieDetailViewController: UIViewController {
	// MARK: - Outlets
	@IBOutlet weak var tagline: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	@IBOutlet weak var budgetLabel: UILabel!
	@IBOutlet weak var publishDateLabel: UILabel!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var detailStackView: UIStackView!

	// MARK: - Properties
	var bookmarkButton = UIButton()
	let bannerAdd = NotificationBanner(title: Localization.bookmarkAdded, subtitle: Localization.bookmarkAddedSubtitle, style: .success)
	let bannerRemove = NotificationBanner(title: Localization.bookmarkRemoved, subtitle: Localization.bookmarkRemovedSubtitle, style: .success)
	var viewModel: MovieDetailViewModel!
	var bookmarkViewModel: BookmarkService!
	public var movieId = 0
	private let disposeBag = DisposeBag()

	// MARK: - Factory init
	public static func createWith(id: Int, viewModel: MovieDetailViewModel, bookmarkViewModel: BookmarkService) -> MovieDetailViewController? {
		guard let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController else {
			return nil
		}
		viewController.movieId = id
		viewController.viewModel = viewModel
		viewController.bookmarkViewModel = bookmarkViewModel
		return viewController
	}

	// MARK: - ViewDidLoad
	override func viewDidLoad() {
		super.viewDidLoad()
		bindViewModel()
		detailStackView.layer.cornerRadius = 5
		configureBookmarkButton()
	}

	// MARK: - Bookmark button configuration
	func configureBookmarkButton() {
		bookmarkButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
		bookmarkButton.tag = 0
		bookmarkButton.tintColor = .yellow
		bookmarkButton.setImage(UIImage(named: "bookmark")?.withRenderingMode(.alwaysOriginal), for: .normal)
		self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: bookmarkButton)]
	}

	// MARK: - UI Bindings
	func bindViewModel() {
		imageView.layer.masksToBounds = true
		imageView.layer.cornerRadius = 10
		titleLabel.lineBreakMode = .byWordWrapping
		titleLabel.numberOfLines = 3

		imageView.layer.masksToBounds = true
		imageView.layer.cornerRadius = 10

		self.viewModel.getMovie(id: movieId)
			.asObservable()
			.map{ $0.title }
			.bind(to: self.titleLabel.rx.text)
			.disposed(by: self.disposeBag)

		self.viewModel.getMovie(id: movieId)
			.asObservable()
			.map{ $0.tagline }
			.bind(to: self.tagline.rx.text)
			.disposed(by: self.disposeBag)

		self.viewModel.getMovie(id: movieId)
			.asObservable()
			.map{ $0.release_date }
			.bind(to: self.publishDateLabel.rx.text)
			.disposed(by: self.disposeBag)

		self.viewModel.getMovie(id: movieId)
			.subscribe( onNext: { [weak self] data in
				if (self?.bookmarkViewModel.bookmarkedItems.contains(data)) == true {
					self?.bookmarkButton.setImage(UIImage(named: "bookmarkFill")?.withRenderingMode(.alwaysOriginal), for: .normal)
					self?.bookmarkButton.tag = 1
				} else {
					self?.bookmarkButton.setImage(UIImage(named: "bookmark")?.withRenderingMode(.alwaysOriginal), for: .normal)
					self?.bookmarkButton.tag = 0
				}
				self?.budgetLabel.text = String("Budget: \(data.budget) USD")
			}).disposed(by: disposeBag)

		self.viewModel.getMovie(id: movieId)
			.asObservable()
			.map { $0.overview }
			.bind(to: self.descriptionLabel.rx.text)
			.disposed(by: disposeBag)

		self.viewModel.getMovie(id: movieId)
			.subscribe( onNext: { [weak self] data in
				self?.imageView.load(url: data.posterURL)
			}).disposed(by: disposeBag)

		self.viewModel.getMovie(id: movieId)
			.subscribe( onNext: { [weak self] data in
				self?.bookmarkViewModel.bookmarkedItem = data
			}).disposed(by: disposeBag)

		self.bookmarkButton.rx.tap
			.subscribe(onNext: { [weak self] data in
				if self?.bookmarkButton.tag == 0 {
					self?.bookmarkButton.setImage(UIImage(named: "bookmarkFill")?.withRenderingMode(.alwaysOriginal), for: .normal)
					self?.bookmarkViewModel.bookmarkedItems.append((self?.bookmarkViewModel.bookmarkedItem)!)
					self?.bookmarkButton.tag = 1
					self?.bannerAdd.show(bannerPosition: .bottom)
				} else {
					self?.bookmarkButton.setImage(UIImage(named: "bookmark")?.withRenderingMode(.alwaysOriginal), for: .normal)
					self?.bookmarkButton.tag = 0
					self?.bookmarkViewModel.bookmarkedItems.removeLast()
					self?.bannerRemove.show(bannerPosition: .bottom)
				}
			}).disposed(by: disposeBag)
	}
}
