//
//  BookmarkListViewController.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 24..
//

import UIKit
import PKHUD
import RxSwift
import RxCocoa

public class BookmarkListViewController: UIViewController {
// MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!

	// MARK: - Properties
	var viewModel: BookmarkService!
	private let disposeBag = DisposeBag()

	public static func createWith(viewModel: BookmarkService) -> BookmarkListViewController? {
		viewModel.bookmarkedItem = MovieDetail(budget: 100, runTime: 100, overview: "test", title: "test", tagline: "", release_date: "", poster_path: "", popularity: 10, vote_average: 10)

		guard let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BookmarkListViewController") as? BookmarkListViewController else {
			return nil
		}
		viewController.viewModel = viewModel
		return viewController
	}

	public override func viewDidLoad() {
		super.viewDidLoad()
		tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
		bindViewModel()
	}

	func bindViewModel() {
		viewModel.bookmarkObservable.bind(to: tableView.rx.items) { tableView, index, element in
				let indexPath = IndexPath(item: index, section: 0)
				guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell else {
					return UITableViewCell()
				}
				cell.titleLabel.text = element.title
				cell.descriptionLabel.text = element.overview
				cell.publishLabel.text = element.release_date
				cell.posterImageView.load(url: element.posterURL)

				return cell
			}.disposed(by: disposeBag)

			tableView.rx.itemDeleted
				.subscribe(onNext: { indexPath in
					self.viewModel.bookmarkedItems.remove(at: indexPath.row)
					self.tableView.reloadData()

				}).disposed(by: disposeBag)
		}
	}

