//
//  MovieListViewController.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import UIKit
import PKHUD
import RxSwift
import RxCocoa

public class MovieListViewController: UIViewController, UISearchBarDelegate {
	// MARK: - Outlets
	@IBOutlet weak var tableView: UITableView!

	// MARK: - Properties

	private let disposeBag = DisposeBag()
	private let refreshControl = UIRefreshControl()

	var bookmarkButton = UIButton()
	var filterButton = UIButton()

	// MARK: - SearchBar
	lazy var searchController: UISearchController = ({
		let controller = UISearchController()
		controller.searchBar.searchBarStyle = UISearchBar.Style.default
		controller.searchBar.placeholder = Localization.searchBarPlaceholder
		controller.searchBar.sizeToFit()
		controller.searchBar.isTranslucent = false
		return controller
	})()

	var viewModel: MovieListViewModel = MovieListViewModel()
	var detailViewModel: MovieDetailViewModel = MovieDetailViewModel()
	var bookmarkViewModel: BookmarkService = BookmarkService()

	// MARK: - Factory init
	static func createWith(viewModel: MovieListViewModel, detailViewModel: MovieDetailViewModel, bookmarkViewModel: BookmarkService) -> MovieListViewController? {
		guard let viewController = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MovieListViewController") as? MovieListViewController else {
			return nil
		}
		viewController.viewModel = viewModel
		viewController.detailViewModel = detailViewModel
		viewController.bookmarkViewModel = bookmarkViewModel
		return viewController
	}

	// MARK: - ViewDidLoad
	public override func viewDidLoad() {
		super.viewDidLoad()
		tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: "MovieCell")
		bindViewModel()
		viewModel.getMovies(query: Localization.defaultQuery)
		self.refreshControl.endRefreshing()
		setupRefreshControl()
		searchController.searchBar.delegate = self
		let searchBar = searchController.searchBar
		tableView.tableHeaderView = searchController.searchBar
		configureBookmarkButton()
		configureFilterButton()

		searchBar.rx.text
			.orEmpty
			.debounce(.milliseconds(500), scheduler: MainScheduler.instance)
			.distinctUntilChanged()
			.bind(to: viewModel.searchValue)
			.disposed(by: disposeBag)
	}


	// MARK: - Bookmark button configuration
	func configureBookmarkButton() {
		bookmarkButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
		bookmarkButton.tag = 0
		bookmarkButton.tintColor = .yellow
		bookmarkButton.setImage(UIImage(named: "bookmarkFill")?.withRenderingMode(.alwaysOriginal), for: .normal)
		self.navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: bookmarkButton)]
	}

	// MARK: - Filter button configuration
	func configureFilterButton() {
		filterButton.frame = CGRect(x: 0, y: 0, width: 44, height: 44)
		filterButton.tintColor = .blue
		filterButton.setImage(UIImage(named: "sort")?.withRenderingMode(.alwaysOriginal), for: .normal)
		self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: filterButton)
	}

	func filtertapped(in vc: UIViewController) {
		let alert = UIAlertController(title: "Sort by:", message: nil, preferredStyle: .actionSheet)
		let popularityAction = UIAlertAction(title: "Popularity", style: .default) { (_) in
			self.viewModel.sort = Sorting.popularity
			self.viewModel.getMovies(query: Localization.defaultQuery)
		}
		let ratingAction = UIAlertAction(title: "Rating", style: .default) { (_) in
			self.viewModel.sort = Sorting.rating
			self.viewModel.getMovies(query: Localization.defaultQuery)
		}

		alert.addAction(popularityAction)

		alert.addAction(ratingAction)

		vc.present(alert, animated: true)
	}

	// MARK: - Refresh
	@objc private func refreshAction() {
		viewModel.getMovies(query: Localization.defaultQuery)
		self.refreshControl.endRefreshing()
	}

	private func setupRefreshControl() {
		// Add Refresh Control to Table View
		if #available(iOS 10.0, *) {
			tableView.refreshControl = refreshControl
		} else {
			tableView.addSubview(refreshControl)
		}

		// Configure Refresh Control
		refreshControl.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
	}

	// MARK: - Bindings
	func bindViewModel() {
		viewModel.movieCells
			.bind(to: self.tableView.rx.items) { tableView, index, element in
			let indexPath = IndexPath(item: index, section: 0)
			switch element {
			case .normal(let viewModel):
				guard let cell = tableView.dequeueReusableCell(withIdentifier: "MovieCell", for: indexPath) as? MovieCell else {
					return UITableViewCell()
				}
				cell.viewModel = viewModel
				return cell
			case .error(message: let message):
				let cell = UITableViewCell()
				cell.isUserInteractionEnabled = false
				cell.textLabel?.text = message
				return cell
			case .empty:
				let cell = UITableViewCell()
				cell.isUserInteractionEnabled = false
				cell.textLabel?.text = Localization.emptyCell
				return cell
			}
		}.disposed(by: disposeBag)

		viewModel
			.onShowLoadingHud
			.map { [weak self] in self?.setLoadingHud(visible: $0) }
			.subscribe()
			.disposed(by: disposeBag)

		tableView.rx.itemSelected
			.subscribe(onNext: { [weak self] indexPath in
				let cell = self?.tableView.cellForRow(at: indexPath) as? MovieCell
				guard let cellId = cell?.viewModel?.id else { return }
				let viewController = MovieDetailViewController.createWith(id: cellId, viewModel: self!.detailViewModel, bookmarkViewModel: self!.bookmarkViewModel)
				self?.navigationController?.pushViewController(viewController!, animated: true)
				self!.tableView.deselectRow(at: indexPath, animated: true)
				self?.searchController.isActive = false
			}).disposed(by: disposeBag)

		bookmarkButton.rx.tap
			.subscribe(onNext: { [weak self] in
				guard let viewController = BookmarkListViewController.createWith(viewModel: self!.bookmarkViewModel) else { return }
				self?.navigationController?.pushViewController(viewController, animated: true)
			}).disposed(by: disposeBag)

		filterButton.rx.tap
			.subscribe(onNext: { [weak self] in
				self?.filtertapped(in: self!)
			}).disposed(by: disposeBag)

	}
}

// MARK: - Extension: LoadingHud
extension MovieListViewController {
	private func setLoadingHud(visible: Bool) {
		PKHUD.sharedHUD.contentView = PKHUDSystemActivityIndicatorView()
		visible ? PKHUD.sharedHUD.show(onView: view) : PKHUD.sharedHUD.hide()
	}
}
