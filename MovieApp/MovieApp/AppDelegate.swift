//
//  AppDelegate.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	/*public let viewModel: MovieListViewModel = MovieListViewModel()
	public let detailViewModel: MovieDetailViewModel = MovieDetailViewModel()
	public let bookmarkService: BookmarkService = BookmarkService()*/

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.


		// Setting view models

		/*self.window = UIWindow(frame: UIScreen.main.bounds)

		guard let movieViewController = MovieListViewController.createWith(viewModel: viewModel, detailViewModel: detailViewModel, bookmarkViewModel: bookmarkService) else { return false }

		let navController = UINavigationController(rootViewController: movieViewController)

		self.window!.rootViewController = navController

		self.window!.makeKeyAndVisible()*/
		return true
	}

	// MARK: UISceneSession Lifecycle

	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		// Called when a new scene session is being created.
		// Use this method to select a configuration to create the new scene with.
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
		// Called when the user discards a scene session.
		// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
		// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
	}


}

