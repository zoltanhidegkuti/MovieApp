//
//  AppServerClient.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import Alamofire
import RxSwift

class AppServerClient {
	enum GetMoviesFailureReason: Int, Error {
		case notFound = 404
	}

	func getMovies(query: String) -> Observable<[Movie]> {
		return Observable.create{ observer -> Disposable in
			AF.request(self.baseURL + query)
				.validate().responseJSON { response in
					switch response.result {
					case .success:
						guard let data = response.data else {
							observer.onError(response.error ?? GetMoviesFailureReason.notFound)
							return
						}
						do {
							let movies = try JSONDecoder().decode(Movies.self, from: data)
							observer.onNext(movies.results!)
						} catch {
							observer.onError(error)
						}
					case .failure(let error):
						if let statusCode = response.response?.statusCode,
						   let reason = GetMoviesFailureReason(rawValue: statusCode)
						{
							observer.onError(reason)
						}
						observer.onError(error)
					}
				}
			return Disposables.create()
		}
	}

	func getMovie(id: Int) -> Observable<MovieDetail> {
		return Observable.create{ observer -> Disposable in
			AF.request(self.baseDetailURL + String(id) + self.detailURLEnd)
				.validate().responseJSON { response in
					switch response.result {
					case .success:
						guard let data = response.data else {
							observer.onError(response.error ?? GetMoviesFailureReason.notFound)
							return
						}
						do {
							let movie = try JSONDecoder().decode(MovieDetail.self, from: data)
							observer.onNext(movie)
						} catch {
							observer.onError(error)
						}
					case .failure(let error):
						if let statusCode = response.response?.statusCode,
						   let reason = GetMoviesFailureReason(rawValue: statusCode)
						{
							observer.onError(reason)
						}
						observer.onError(error)
					}
				}
			return Disposables.create()
		}
	}

	// MARK: - Constants
	let baseURL = "https://api.themoviedb.org/3/search/movie?api_key=555dd34b51d2f5b7f9fdb39e04986933&query="
	let baseDetailURL = "https://api.themoviedb.org/3/movie/"
	let detailURLEnd = "?api_key=555dd34b51d2f5b7f9fdb39e04986933"
}
