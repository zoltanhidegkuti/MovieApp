//
//  BookmarkListViewModel.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 24..
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift

public class BookmarkService {
// MARK: - Public properties

	var bookmarkedMovies = BehaviorRelay<[MovieDetail]>(value: [])
	lazy var bookmarkObservable: Observable<[MovieDetail]> = self.bookmarkedMovies.asObservable()

	init() {
	}

	public var bookmarkedItems: [MovieDetail] = [] {
		didSet {
			bookmarkedMovies.accept(bookmarkedItems)
		}
	}
	public var bookmarkedItem: MovieDetail!
}
