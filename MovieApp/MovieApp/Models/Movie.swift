//
//  Movie.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import UIKit
import RealmSwift

public struct Movies: Equatable, Codable {
	public var results: [Movie]? = []
}

// MARK: - Movie model
public struct Movie: Equatable, Codable {

	public var id: Int
	public var title: String?
	public var overview: String?
	public var release_date: String?
	public var poster_path: String?
	public var popularity: Double?
	public var vote_average: Double?
	public var posterURL: URL {
		return URL(string: "https://image.tmdb.org/t/p/w500\(poster_path ?? "")") ?? URL(string: "") as! URL
	}
}

// MARK: - Movie detail model
public struct MovieDetail: Equatable, Codable {
	public var budget: Int
	public var runTime: Int?
	public var overview: String?
	public var title: String?
	public var tagline: String?
	public var release_date: String?
	public var poster_path: String?
	public var popularity: Double?
	public var vote_average: Double?
	public var posterURL: URL {
		return URL(string: "https://image.tmdb.org/t/p/w500\(poster_path ?? "")") ?? URL(string: "") as! URL
	}
	/*public var posterImage: UIImage {
		let url = URL(string: "https://image.tmdb.org/t/p/w500/jBUukLTCmXL79itbmK5JtQvImYS.jpg")!
		var returnImage = UIImage()
		if let data = try? Data(contentsOf: url) {
			if let image = UIImage(data: data) {
				DispatchQueue.main.async {
					returnImage = image
				}
			}
		}
		return returnImage
	}*/
}

class MovieDetailObject: Object {
	@objc dynamic public var budget: Int = 0
	@objc dynamic public var runTime: Int = 0
	@objc dynamic public var overview: String? = ""
	@objc dynamic public var title: String? = ""
	@objc dynamic public var tagline: String? = ""
	@objc dynamic public var release_date: String? = ""
	@objc dynamic public var poster_path: String? = ""
	@objc dynamic public var popularity: Double = 0
	@objc dynamic public var vote_average: Double = 0
	@objc dynamic public var posterURL: URL {
		return URL(string: "https://image.tmdb.org/t/p/w500\(poster_path ?? "")") ?? URL(string: "") as! URL
	}
}






