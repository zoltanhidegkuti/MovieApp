//
//  Alert.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import UIKit

struct AlertAction {
	let buttonTitle: String
	let handler: (() -> Void)?
}

struct SingleButtonAlert {
	let title: String
	let message: String?
	let action: AlertAction
}


