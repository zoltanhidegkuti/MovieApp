//
//  Extensions.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import UIKit

extension UIImageView {
	// MARK: - ImageView load with URL
	func load(url: URL) {
		DispatchQueue.global().async { [weak self] in
			if let data = try? Data(contentsOf: url) {
				if let image = UIImage(data: data) {
					DispatchQueue.main.async {
						self?.image = image
					}
				}
			}
		}
	}
}

extension UIStoryboard {
	// Easily initiate view controller (for testing,...)
	static func viewController<T: UIViewController>(from storyboardName: String, type: T.Type) -> T {
		let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
		return storyboard.instantiateViewController(withIdentifier: String(describing: type)) as! T
	}
}
