//
//  MovieDetailViewModel.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import RxSwift
import RxCocoa

class MovieDetailViewModel {
	// MARK: - Public properties
	public var id = 0
	var onShowLoadingHud: Observable<Bool> {
		return loadInProgress
			.asObservable()
			.distinctUntilChanged()
	}

	let appServerClient: AppServerClient
	let disposeBag = DisposeBag()

	// MARK: - Private properties
	private let loadInProgress = BehaviorRelay(value: false)

	// MARK: - Init
	init(appServerClient: AppServerClient = AppServerClient()) {
		self.appServerClient = appServerClient
	}

	// MARK: - Request
	func getMovie(id: Int) -> Observable<MovieDetail> {
		loadInProgress.accept(true)
		let selectedMovie = appServerClient.getMovie(id: id)
		return selectedMovie
	}
}


