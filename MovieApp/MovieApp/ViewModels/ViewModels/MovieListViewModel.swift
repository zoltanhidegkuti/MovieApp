//
//  MovieViewModel.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import RxSwift
import RxCocoa
import RealmSwift

enum MovieTableViewCellType {
	case normal(cellViewModel: MovieCellViewModel)
	case error(message: String)
	case empty
}

enum Sorting {
	case rating, popularity
	var sorter: (Movie, Movie) -> Bool {
		switch self {
		case .popularity:
			return {
				$0.popularity ?? 0.0 > $1.popularity ?? 0.0 }
		case .rating:
			return { $0.vote_average ?? 0.0 > $1.vote_average ?? 0.0 }
		}
	}
}

class MovieListViewModel {
	// MARK: - Properties
	var movieCells: Observable<[MovieTableViewCellType]> {
		return cells.asObservable()
	}
	var onShowLoadingHud: Observable<Bool> {
		return loadInProgress
			.asObservable()
			.distinctUntilChanged()
	}

	var searchValue: BehaviorRelay<String> = BehaviorRelay(value: "")
	lazy var searchValueObservable: Observable<String> = self.searchValue.asObservable()

	let onShowError = PublishSubject<SingleButtonAlert>()
	let appServerClient: AppServerClient
	public var sort = Sorting.rating
	let disposeBag = DisposeBag()

	// MARK: - Private properties
	private let loadInProgress = BehaviorRelay(value: false)
	private let cells = BehaviorRelay<[MovieTableViewCellType]>(value: [])

	// MARK: - Init
	init(appServerClient: AppServerClient = AppServerClient()) {
		self.appServerClient = appServerClient



		searchValueObservable
			.filter { $0.count > 0 }
			.subscribe(onNext: { (value) in
				self.getMovies(query: value)
			})
			.disposed(by: disposeBag)
	}

	// MARK: - Movie list request
	func getMovies(query: String) {
		loadInProgress.accept(false)

		appServerClient.getMovies(query: query)
			.subscribe(
				onNext: {[weak self] movies in
					self?.loadInProgress.accept(false)
					guard movies.count > 0 else {
						self?.cells.accept([.empty])
						return
					}
					guard let sortEnum = self?.sort.sorter else { return }
					self?.cells.accept(movies
										.sorted(by: sortEnum)
										.compactMap {
						.normal(cellViewModel:
									MovieCellViewModel(movie: $0 ))
					})
				},
				onError: { [weak self] error in
					self?.loadInProgress.accept(false)
					self?.cells.accept([
						.error(
							message: (error as? AppServerClient.GetMoviesFailureReason)?.getErrorMessage() ?? Localization.requestFailed
						)
					])
				}
			)
			.disposed(by: disposeBag)
	}
}

// MARK: - AppServerClient.GetMoviesFailureReason
fileprivate extension AppServerClient.GetMoviesFailureReason {
	func getErrorMessage() -> String? {
		switch self {
		case .notFound:
			return Localization.notFound
		}
	}
}
