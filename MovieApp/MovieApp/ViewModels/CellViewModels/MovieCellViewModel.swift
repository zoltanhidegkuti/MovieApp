//
//  MovieCellViewModel.swift
//  MovieApp
//
//  Created by Hidegkuti Zoltán on 2022. 02. 21..
//

import Foundation
import UIKit

public class MovieCellViewModel {
	// MARK: - Cell ViewModel
	private let movie: Movie

	public let title: String
	public let id: Int
	public let release_date: String
	public let poster_Path: URL
	public let overview: String
	public let popularity: Double
	public let rating: Double

	// MARK: - Init
	public init(movie: Movie) {
		self.movie = movie
		popularity = movie.popularity ?? 0.0
		rating = movie.vote_average ?? 0.0
		overview = movie.overview ?? "Test description"
		title = movie.title ?? "Test title"
		id = movie.id
		release_date = movie.release_date ?? "2022.02.21."
		poster_Path = movie.posterURL
	}
}
